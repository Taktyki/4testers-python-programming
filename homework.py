def does_the_pet_require_a_vaccine(pets_type: str, pets_age: int):
    if  pets_type == 'dog' and (pets_age - 1)  % 2 == 0:
        return True
    elif pets_type == 'cat' and (pets_age - 1)  % 3 == 0:
        return True
    elif pets_type != 'cat' and pets_type != 'dog':
        raise ValueError("No info")
    else:
        return False


if __name__ == '__main__':
    pets_type = input('Enter pet\'s type:')
    try:
        pets_age = int(input('Enter pet\'s age:'))
        print(does_the_pet_require_a_vaccine(pets_type, pets_age))
    except ValueError:
        print('Pet\'s age needs to be a number')
    except NameError:
        print('Pet\'s age needs to be a number')