import random
import string


def create_dictionary_of_random_person_data():
    random_string = ''.join(random.choice(string.ascii_lowercase) for _ in range(6, 12))
    person = {
        "email": f"{random_string}@example.com",
        "seniority_years": random.randint(0, 40),
        "female": bool(random.getrandbits(1))
    }
    return person


def create_dictionary_of_person_data(email: str, seniority_years: int, is_female: bool):
    person = {
        "email": f"{email}@example.com",
        "seniority_years": seniority_years,
        "female": is_female
    }
    return person


def create_list_of_dictionaries_of_person_data(list_length: int):
    list_of_dictionaries_of_person_data = []
    for i in range(list_length):
        list_of_dictionaries_of_person_data.append(create_dictionary_of_random_person_data())
    return list_of_dictionaries_of_person_data


def filter_emails_for_seniority_years_higher_than_10(list_of_dictionaries_of_person_data: list):
    filtered_email_list = []
    for dictionary in list_of_dictionaries_of_person_data:
        if dictionary["seniority_years"] > 10:
            filtered_email_list.append(dictionary["email"])
    return filtered_email_list


def filter_female_on_the_list(list_of_dictionaries_of_person_data: list):
    filtered_list_of_dictionaries_of_person_data = []
    for dictionary in list_of_dictionaries_of_person_data:
        if dictionary["female"] == True:
            filtered_list_of_dictionaries_of_person_data.append(dictionary)
    return filtered_list_of_dictionaries_of_person_data


if __name__ == '__main__':
    '''for i in range(10):
        print(create_dictionary_of_random_person_data())

    for i in range(10):
        email = ''.join(random.choice(string.ascii_lowercase) for _ in range(6, 12))
        seniority_years = random.randint(0, 40)
        is_female = bool(random.getrandbits(1))
        print(create_dictionary_of_person_data(email, seniority_years, is_female))'''
    list_to_filter = create_list_of_dictionaries_of_person_data(10)
    print(list_to_filter)
    print("\n")
    # print(filter_emails_for_seniority_years_higher_than_10(list_to_filter))
    # print(filter_female_on_the_list(list_to_filter))
    # print(filter_female_on_the_list(list_to_filter))
