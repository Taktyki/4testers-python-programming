import random


def name():
    for i in range(1, 31):
        if i % 7 == 0:
            print(i)


def print_random_numbers(n):
    for i in range(n):
        print(random.randint(1, 1000))


def list_of_ten_numbers_between_1000_and_5000() -> list:
    list_of_numbers = []
    for i in range(10):
        list_of_numbers.append(random.randint(1000, 5000))
    return list_of_numbers

def print_each_number_in_the_list_squared(list: list):
    for element in list:
        print(element ** 2)

def map_temperature_in_celcius_to_fahrenheit(list_of_temperatures_in_celcius: list):
    list_of_temperatures_in_fahrenheit = []
    for temperature in list_of_temperatures_in_celcius:
        list_of_temperatures_in_fahrenheit.append((temperature * 1.8) + 32)
    return list_of_temperatures_in_fahrenheit

def filter_grades_greater_or_equal_3(grades):
    acceptable_grades = []
    for grade in acceptable_grades:
        if grade >= 3:
            acceptable_grades.append(grade)
    return acceptable_grades

if __name__ == '__main__':
    name()
    #print_random_numbers(10)
    #list_of_ints = list_of_ten_numbers_between_1000_and_5000()
    #print_each_number_in_the_list_squared(list_of_ints)
    #list_of_temperatures_in_celcius = [10.3, 23.2, 15.8, 19.0, 14.0, 23.0, 25.0]
    #print(map_temperature_in_celcius_to_fahrenheit(list_of_temperatures_in_celcius))



