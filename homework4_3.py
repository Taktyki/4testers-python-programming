from zadanie import female_fnames, male_fnames, surnames, countries
import random
import datetime


def create_a_list_of_personal_data():
    list_of_personal_data = []
    for i in range(10):
        if i < 5:
            firstname = random.choice(female_fnames)
        else:
            firstname = random.choice(male_fnames)
        surname: str = random.choice(surnames)
        age = random.randint(5, 45)
        adult: bool = False
        if age >= 18:
            adult = True
        birth_year = datetime.datetime.now() - datetime.timedelta(days=365 * age)
        person = {
            "firstname": firstname,
            "lastname": surname,
            "country": random.choice(countries),
            "email": f"{firstname.lower()}.{surname.lower()}@example.com",
            "age": age,
            "adult": adult,
            "birth_year": birth_year.strftime('%Y')
        }
        list_of_personal_data.append(person)
    return list_of_personal_data


if __name__ == '__main__':
    list_of_people_personal_data = create_a_list_of_personal_data()
    for person in list_of_people_personal_data:
        print(
            f"Hi! I'm {person["firstname"]} {person["lastname"]}. I come from {person["country"]} and I was born in {person["birth_year"]}")
