from homework import does_the_pet_require_a_vaccine


def test_cat_age_one_requires_vaccinastions():
    assert does_the_pet_require_a_vaccine("dog", 1) == True


def test_dog_age_one_requires_vaccinastions():
    assert does_the_pet_require_a_vaccine("cat", 1) == True


def test_dog_age_3_requires_vaccinastions():
    assert does_the_pet_require_a_vaccine("dog", 3) == True
