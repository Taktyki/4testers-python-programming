def print_area_of_a_circle_with_radius(r: str):
    area_of_a_circle = 3.1415 * r ** 2
    print(area_of_a_circle)


def list_of_three_highest(number_list: list):
    i = 3
    list_of_3_highest = number_list[:3]
    while i < len(number_list):
        if number_list[i] > list_of_3_highest[0]:
            list_of_3_highest.insert(0, number_list[i])
        elif number_list[i] > list_of_3_highest[1]:
            list_of_3_highest.insert(1, number_list[i])
        elif number_list[i] > list_of_3_highest[2]:
            list_of_3_highest.insert(2, number_list[i])
        i = i + 1
    return list_of_3_highest[:3]


low = {
    "high": "bar",

}

random_numbers = [9, 5, 33, 5, 87, 5435, 3, 6, 9, 33, 5, 6, 7, 8]
numbers_in_order = list_of_three_highest(random_numbers)

if __name__ == '__main__':
    # print_area_of_a_circle_with_radius(number)
    print(numbers_in_order)
