friends_name = "Przemysław"
friends_age = 40
friends_pet_amount = 0
has_driving_license = True
friendship_years = 17

print("Name:", friends_name, sep="\n")
print("Age:", friends_age, sep="\n")
print("Pet amount:", friends_pet_amount, sep="\n")
print("Has driving license:", has_driving_license, sep="\n")
print("Friendship years:", friendship_years, sep="\n")
print("My friends name is "+friends_name+". He is "+str(friends_age+1-1)+" years old. He has "+str(friends_pet_amount)+" pets. If I say, that he has a driving license, then it would be "+str(has_driving_license)+". We know each others for nearly "+str(friendship_years)+" years.")

print("------------------------------------------")

print("My friends name is",friends_name,". He is ",friends_age,"years old. He has",friends_pet_amount,"pets. If I say, that he has a driving license, then it would be",has_driving_license,". We know each others for nearly",friendship_years,"years.")

print("------------------------------------------")

print(
    "Name:", friends_name,
    "Age:", friends_age,
    sep=" wooof "
)

name_surname = "Bartek Tyszkiewicz"
email = "mojMail@gmail.com"
phone_number = "+48111222333"


bio = "Name: " + name_surname + "\nEmail: " + email + "\nPhone: " + phone_number

print("Name:", name_surname, "\nEmail:", email, "\nPhone:", phone_number)
print(bio)

bio_smarter = f"Name: {name_surname}\nEmail: {email}\nPhone: {phone_number}"
print(bio_smarter)

bio_docstring = f"""Name: {name_surname}
Email: {email}
Phone: {phone_number}"""
print(bio_docstring)